:title: Help
:date: 2013-09-20
:lang: en


You can get help with CsoundQt on the main Csound Mailing List, but for CsoundQt specific issues, you can join the `CsoundQt Users Mailing List <https://lists.sourceforge.net/lists/listinfo/qutecsound-users>`_, for ideas, bug reports and suggestions. This mailing list is also available as a `Forum on Nabble <http://qutecsound-users.829572.n3.nabble.com/>`_.

Documentation
-------------
Currently there is only a `Quick Reference Guide for CsoundQt <http://sourceforge.net/projects/qutecsound/files/Documentation/Quick%20Reference/QuteCsoundQuickRef.zip/download>`_, but hopefully CsoundQt is intuitive enough that it should be enough to get you started. This guide is also available on the Help menu in the CsoundQt application.


However there are two excellent chapters in the `Csound FLOSS manual <http://en.flossmanuals.net/csound/>`_:

 * http://en.flossmanuals.net/csound/qutecsound/ describes in detail the configuration menu (of version 0.7.3)

 * http://en.flossmanuals.net/csound/c-python-in-csoundqt describes how to use Python in CsoundQt.


Wiki
----
The `CsoundQt Wiki <https://sourceforge.net/p/qutecsound/wiki/Home/>`_ is open for contributions. Have a look at the `old CsoundQt Wiki <http://sourceforge.net/apps/mediawiki/qutecsound/>`_, but please avoid adding information there as it might be removed in the future.

Bug and feature reports
-----------------------
Errors can be filed in the `Bug tracker <https://sourceforge.net/p/qutecsound/bugs/>`_. New feature suggestions can be filed in the `Feature tracker <https://sourceforge.net/p/qutecsound/new-features/>`_.

Video tutorials
---------------
Alex Hofmann has created a series of video tutorials for CsoundQt:

.. youtube:: P5OOyFyNaCA
.. youtube:: 0XcQ3ReqJTM
.. youtube:: KgYea5s8tFs
.. youtube:: 8zszIN_N3bQ

Other video tutorials:

.. youtube:: KKlCTxmzcS0
.. youtube:: O9WU7DzdUmE

