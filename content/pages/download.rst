:title: Download
:date: 2013-09-20
:lang: en


Installation
============

The easiest way to get CsoundQt is to download `Csound <https://sourceforge.net/projects/csound/files/csound6/>`_ which includes CsoundQt. However, there are pre-built binaries and nightly builds for OS X on the `Sourceforge Files page <https://sourceforge.net/projects/qutecsound/files/CsoundQt/>`_. The nightly build server is kindly provided by the `Institute for Computer Music and Sound Technology (ICST) <http://www.icst.net/>`_ at ZHDK.

It's also relatively easy to `build CsoundQt <|filename|dev.md>`_.
